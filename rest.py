import os
from flask import Flask
app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    nmap='/run.sh ' + path
    result_code = os.system(nmap + ' > output.txt')
    if os.path.exists('output.txt'):
        fp = open('output.txt', "r")
        output = fp.read()
        fp.close()
        os.remove('output.txt')
    return '%s' % output

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)