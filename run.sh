#!/bin/bash

root_dir=/shared/

report_extension="html"

if [[ ! -z $format ]]
then
    report_extension=$format
fi

xml_dir=xml_files/
report_file=reports/report.$report_extension

function get_filename(){
    echo $1 | tr / -
}

if [ $# -eq 1 ]; then
 line=$1
else
 line=`python3 url.py`
fi

if [ -e $root_dir$xml_dir ]; then
  rm -rf $root_dir$xml_dir/*
else
  mkdir -p $root_dir$xml_dir
fi

if [ -e $root_dir$report_file ]; then
	rm -f $root_dir$report_file
fi

filename=$(get_filename $line)".xml"
echo "line = $line"
nmap -sV -oX $root_dir$xml_dir/$filename -oN - -v1 --script=vulners/vulners.nse $line

echo $line >  /shared/ips.txt

format="html" python /output_report.py $root_dir$xml_dir $root_dir$report_file /shared/ips.txt