FROM python:3.9.0

RUN apt-get update \
     && apt-get install -y --no-install-recommends \
      nmap git bash \
      systemd openssh-server \
    && rm -rf /var/lib/apt/lists/*

# Authorize SSH Host
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan github.com > /root/.ssh/known_hosts && \
    ssh-keyscan git.npu.world >> /root/.ssh/known_hosts 

# Add the keys and set permissions
ADD config/id_ed25519 /root/.ssh/id_ed25519
ADD config/id_ed25519.pub /root/.ssh/id_ed25519.pub
ADD config/authorized_keys /root/.ssh/authorized_keys
ADD config/docker-entrypoint.sh /docker-entrypoint.sh


RUN chmod 600 /root/.ssh/id_ed25519 && \
    chmod 600 /root/.ssh/id_ed25519.pub  && \
    chmod 600 /root/.ssh/authorized_keys

COPY requirements.txt /
RUN pip install --no-cache-dir -r requirements.txt

RUN git clone https://github.com/vulnersCom/nmap-vulners /usr/share/nmap/scripts/vulners && nmap --script-updatedb
RUN mkdir /shared

COPY run.sh output_report.py gcp_push.py aws_push.py /
COPY contrib /contrib
COPY shared /shared
COPY url.py /
COPY rest.py /
COPY scan.sh /

EXPOSE 8000 22

ENTRYPOINT ["python3", "rest.py"]